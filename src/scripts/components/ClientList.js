'use strict';

import React from 'react';
import ClientData from './ClientData';

export default ({ data, update }) => {
    if (!data) { return (<p>Loading...</p>); }

    const clients = data.map((client, index) => {
        return (<ClientData client={client} key={`client-${index}`} update={update} />);
    });

    return (
        <div>
            {clients}
        </div>
    );
};
