import React from 'react';

export default ({ client, update }) => {
    return (
        <div onClick={() => update({ activeIndex: client.index })} className="row border border-dark">
            <div className="col-md-3"><img src={client.general.avatar} className="user-image w-100" /></div>
            <div className="col-md-9">
                <div>
                    <b>Firstname:</b> {client.general.firstName}
                    <b> Lastname:</b> {client.general.lastName}
                </div>
                <div><b>Job:</b> {client.job.title}</div>
            </div>
        </div>
    );
};