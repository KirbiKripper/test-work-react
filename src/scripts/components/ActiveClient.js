import React from 'react';

export default ({ data, activeIndex }) => {

    let client;

    if (data && typeof data === 'object') {
        client = data[0];
        data.forEach((element) => {
            if (element.index === activeIndex) {
                client = element;
            }
        });
    }

    if (!data || !client) {
        return <h3>Nothing found</h3>;
    }

    return (
        <div className="thumbnail">
            <img src={client.general.avatar} />

            <div className="thumbnail-caption">
                <h3>{client.general.firstName + ' ' + client.general.lastName}</h3>
                <div className="row">
                    <div className="col-md-4"><b>Company name:</b></div>
                    <div className="col-md-8">{client.job.company}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Company title:</b></div>
                    <div className="col-md-8">{client.job.title}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Email address:</b></div>
                    <div className="col-md-8">{client.contact.email}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Phone number:</b></div>
                    <div className="col-md-8">{client.contact.phone}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Country:</b></div>
                    <div className="col-md-8">{client.address.country}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>City:</b></div>
                    <div className="col-md-8">{client.address.city}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Street:</b></div>
                    <div className="col-md-8">{client.address.street}</div>
                </div>
                <div className="row">
                    <div className="col-md-4"><b>Zip code:</b></div>
                    <div className="col-md-8">{client.address.zipCode}</div>
                </div>
            </div>
        </div>
    );
};