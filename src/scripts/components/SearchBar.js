import React from 'react';

export default ({ term, data, update }) => {

    const dataSearch = e => {
        const value = e.target.value.toLowerCase();

        const filter = data.filter(client => {
            let fields = [
                    client.general.firstName,
                    client.general.lastName,
                    client.job.company,
                    client.job.title,
                    client.address.street,
                    client.address.country,
                    client.address.city,
                    client.address.zipCode,
                    client.contact.email,
                ],
                isSearchResult = false;
            fields.forEach(function (field) {
                if (field.toLowerCase().includes(value)) {
                    isSearchResult = true;
                }
            });
            return isSearchResult;
        });

        update({
            data: filter,
            term: value
        });

    };

    return (
        <div className="searchbar form-group">
            <input
                value={term}
                type="text"
                className="form-control"
                placeholder="Search people by name..."
                onChange={dataSearch}
            />
        </div>
    );
};