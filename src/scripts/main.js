'use strict';

import React from "react";
import ReactDOM from "react-dom";
import App from './App';

let element = document.getElementById('root');
ReactDOM.render(<App/>, element);