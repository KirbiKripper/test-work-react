import React from 'react';
import load from './utils/load';
import ActiveClient from './components/ActiveClient';
import ClientList from './components/ClientList';
import SearchBar from './components/SearchBar';

const clientsPatch = '../clients.json';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            activeIndex: 0,
            term: ''
        };
        this.loadData();
    }

    loadData() {
        load(clientsPatch).then(clients => {
            this.initialData = [];
            JSON.parse(clients).forEach((client, index) => {
                client.index = index;
                this.initialData.push(client);
            });
            this.setState({
                data: this.initialData
            });
        });
    }

    updateData(config) {
        this.setState(config);
    }

    render() {
        return (
            <div className="app container-fluid">
                <div className="row">
                    <div className="col-md-4">
                        <SearchBar
                            term={this.state.term}
                            data={this.initialData}
                            update={this.updateData.bind(this)}
                        />
                        <ClientList data={this.state.data} update={this.updateData.bind(this)} />
                    </div>
                    <div className="col-md-8">
                        <ActiveClient data={this.state.data} activeIndex={this.state.activeIndex} />
                    </div>
                </div>
            </div>
        );
    }
}