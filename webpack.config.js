let webpack = require('webpack');
let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/scripts/main.js',
    output: {
        filename: 'build.js'
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    "presets": ['@babel/preset-env', '@babel/preset-react']
                }
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    }
                ],
                include: /node_modules/
            },
        ],
    },
    resolve: {
        alias: {
            'semantic-ui': path.join("node_modules", "semantic-ui-css", "semantic.js"),
        },
        extensions: ['.js', '.jsx']
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: "body"
        })],
    optimization: {
        minimize: false
    },
};